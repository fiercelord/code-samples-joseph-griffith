// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com


// A reference to our box2d world
var world;

// A list we'll use to track fixed objects
var boundaries = [];
// A list for all of our rectangles
var pops = [];

var sucks = [];

var particle = [];

var vmax = 5;



function setup() {
  createCanvas(640,360);

  // Initialize box2d physics and create the world
  world = createWorld();

  // Add a bunch of fixed boundaries
  boundaries.push(new Boundary(width/2,-8,width,15,-2));
  boundaries.push(new Boundary(width/2,height+8,width,10,0));
  boundaries.push(new Boundary(width+5,height/2,10,height,0));
  boundaries.push(new Boundary(-7,height/2,10,height,0));
  
  // Add particles
      var rw = random(50, width-10);
      var rh = random(50 ,height-10);
  var n = 15;
  for(var i = 0; i< n;i++){
          var rw = random(50, width-10);
      var rh = random(50 ,height-10);
    var p = (new Lollipop(rw, rh));
    pops.push(p);
  }
  
    for (var i = 0; i < n; i++) {
            var rw = random(50, width-10);
      var rh = random(50 ,height-10);
    var s = new Sucker(rw, rh)
    sucks.push(s);
  }  
}

function draw() {
  background(51);

  // We must always step through time!
  var timeStep = 1.0/30;
  // 2nd and 3rd arguments are velocity and position iterations
  world.Step(timeStep,10,10);

  // Display all the boundaries
  for(var i = 0; i < boundaries.length; i++) {
    boundaries[i].display();
  }

    for(var j = 0; j<pops.length; j++){
  pops[j].display();
  }

  for(var i =0; i<sucks.length; i++){
  sucks[i].display();
  }
  
    for(var i =0; i<particle.length; i++){
  particle[i].display();
  }
  
  collisionReaction();
  if(particle.length>1){
  collisionProduct();
  }
}

function collisionReaction(){
  var sRad = sucks[0].body.m_fixtureList.m_shape.m_radius;
  var pRad = pops[0].body.m_fixtureList.m_shape.m_radius;
      
  for(var i = 0; i<pops.length-1; i++){
    
      sucks.forEach(function(element){
        
          var d = dist(element.positionX(),element.positionY(),pops[i].positionX(),pops[i].positionY());
      if(d<= sRad + pRad){
        
        var newX = element.positionX();
        var newY = element.positionY();
        var newVX = element.velocityX() + pops[i].velocityX();
        var newVY = element.velocityY() + pops[i].velocityY();
        
        pops[i].killBody();
        pops.splice(i, 1);
        element.killBody();
        sucks.splice(sucks.indexOf(element),1);
        var part = new Particle(newX, newY, newVX, newVY);
        particle.push(part);
      }
    });
  }
}
    function collisionProduct(){
      var pRad = particle[0].body.m_fixtureList.m_shape.m_radius;
    for(var i = 0; i<particle.length-1; i++){
    var d = dist(particle[i].positionX(), particle[i].positionY(), particle[i+1].positionX(), particle[i+1].positionY());

      if(d<=pRad*2.01){
        
        var newX = particle[i].positionX();
        var newY = particle[i].positionY();
        var newVX = particle[i].velocityX() + particle[i+1].velocityX();
        var newVY = particle[i].velocityY() + particle[i+1].velocityY();
        
        particle[i].killBody();
        particle.splice(i, 1);
        particle[i].killBody();
        particle.splice(i, 1);
      var p = new Lollipop(newX, newY);
      var s = new Sucker(newX+1,newY+1);
      pops.push(p);
      sucks.push(s);
      }
    
    }
    
    
}
