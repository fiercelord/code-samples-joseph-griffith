// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com

// A circular particle

// Constructor
function Particle(x,y, vx, vy) {
  this.r = 10;

  this.col = color(500);
  

  // Define a body
  var bd = new box2d.b2BodyDef();
  bd.type = box2d.b2BodyType.b2_dynamicBody;
  bd.position = scaleToWorld(x,y);

  // Define a fixture
  var fd = new box2d.b2FixtureDef();
  // Fixture holds shape
  fd.shape = new box2d.b2CircleShape();
  fd.shape.m_radius = scaleToWorld(this.r);
  fd.shape.position = scaleToWorld(x,y);
  

  // Some physics
  fd.density = 1.0;
  fd.friction = 0.0;
  fd.restitution = 1.0;

  // Create the body
  this.body = world.CreateBody(bd);
  // Attach the fixture
  this.body.CreateFixture(fd);

  // Some additional stuff
  this.body.SetLinearVelocity(new box2d.b2Vec2(vx, vy));
  this.body.SetAngularVelocity(random(-5,5));

    this.body.SetUserData(this);

    this.positionX = function(){
var pos =  this.body.GetPosition().x;
  return pos;
}
  
this.positionY = function(){
var pos =  this.body.GetPosition().y;
  return pos;
}
  
    this.velocityX = function(){
    return this.body.GetLinearVelocity().x;
  }

  this.velocityY = function(){
    return this.body.GetLinearVelocity().y;
  }  
  

  // This function removes the particle from the box2d world
  this.killBody = function() {
    world.DestroyBody(this.body);
  };


  // Drawing the box
  this.display = function() {
    // Get the body's position
    var pos = scaleToPixels(this.body.GetPosition());
    // Get its angle of rotation
    var a = this.body.GetAngleRadians();

    // Draw it!
    rectMode(CENTER);
    push();
    translate(pos.x,pos.y);
  //  rotate(a);
    fill(this.col);
    stroke(0);
    strokeWeight(2);
    ellipse(0,0,this.r*2,this.r*2);
    // Let's add a line so we can see the rotation

    pop(); 
  };
}