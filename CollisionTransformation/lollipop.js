// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com

// A rectangular box


// Constructor
function Lollipop(x, y) {
  this.w = 8;
  this.h = 24;
  this.r = 8;

  // Define a body
  var bd = new box2d.b2BodyDef();
  bd.type = box2d.b2BodyType.b2_dynamicBody;
  bd.position = scaleToWorld(x,y);

  // Define fixture #1
  var fd1 = new box2d.b2FixtureDef();
  
  // Fixture holds shape
  fd1.shape = new box2d.b2CircleShape();
  fd1.shape.m_radius = scaleToWorld(this.r);
  fd1.shape.position = scaleToWorld(x,y);
  fd1.density = 1.0;
  fd1.friction = 0.0;
  fd1.restitution = 1.0;
  
  // Create the body
  this.body = world.CreateBody(bd);
  // Attach the fixture
  this.body.CreateFixture(fd1);

  // Some additional stuff
  this.body.SetLinearVelocity(new box2d.b2Vec2(random(-5, 5), random(2, 5)));
    this.body.SetAngularVelocity(random(-5,5));

  // This function removes the particle from the box2d world
  this.killBody = function() {
    world.DestroyBody(this.body);

  };

  //find the particles x and y position
  this.positionX = function(){
var pos =  this.body.GetPosition().x;
  return pos;
}
  
this.positionY = function(){
var pos =  this.body.GetPosition().y;
  return pos;
}
  
    this.velocityX = function(){
    var v = this.body.GetLinearVelocity().x;
      return v;
  }

  this.velocityY = function(){
    var v = this.body.GetLinearVelocity().y;
      return v;
  } 

  // Drawing the box
  this.display = function() {
    // Get the body's position
    var pos = scaleToPixels(this.body.GetPosition());
    // Get its angle of rotation
    var a = this.body.GetAngleRadians();

    // Draw it!
    rectMode(CENTER);
    push();
    translate(pos.x, pos.y);
    rotate(a);
    fill(127);
    stroke(200);
    strokeWeight(2);

 //   rect(0,0,this.w,this.h);
    ellipse(0,0,this.r*2,this.r*2);
    //ellipse(0, -this.h/2, this.r*2, this.r*2);
    pop();
  };

}