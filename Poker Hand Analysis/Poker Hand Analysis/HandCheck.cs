﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poker_Hand_Analysis.Poker_Hands;

namespace Poker_Hand_Analysis
{
    public class HandCheck
    {
        private string[] hand;
        private HandElements[] handCheck = new HandElements[9];

        public string[] Hand
        {
            get;
        }

        public HandCheck(string[] hand)
        {
            this.hand = hand;

            StraightFlush straightFlush = new StraightFlush(hand);
            FourOfAKind fourOfAKind = new FourOfAKind(hand);
            FullHouse fullHouse = new FullHouse(hand);
            Flush flush = new Flush(hand);
            Straight straight = new Straight(hand);
            ThreeOfAKind threeOfAKind = new ThreeOfAKind(hand);
            TwoPair twoPair = new TwoPair(hand);
            Pair pair = new Pair(hand);
            HighCard highCard = new HighCard(hand);

            handCheck[0] = highCard;
            handCheck[1] = pair;
            handCheck[2] = twoPair;
            handCheck[3] = threeOfAKind;
            handCheck[4] = straight;
            handCheck[5] = flush;
            handCheck[6] = fullHouse;
            handCheck[7] = fourOfAKind;
            handCheck[8] = straightFlush;
        }

        public string handName()
        {
            for(int i = 0; i < handCheck.Length; i++)
            {
                if (handCheck[i].isThisHand())
                {
                    return handCheck[i].Name;
                }
            }
            return "";
        }
        public decimal handPowerValue()
        {
            decimal result = 0m;
            for(int i = 0; i < handCheck.Length; i++)
            {
                if (handCheck[i].isThisHand())
                {
                    decimal subPowerValue = 0m;
                    subPowerValue = handCheck[i].subPowerRankingCalculator();
                    result += handCheck[i].PowerRanking;
                    result += subPowerValue;
                    break;
                }
            }
            return result;
        }
    }
}
