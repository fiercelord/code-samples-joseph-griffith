﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poker_Hand_Analysis.Poker_Hands;

namespace Poker_Hand_Analysis
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] sampleHandA = { "4S", "AH", "2D", "3D", "3H" }; //test hand A
            string[] sampleHandB = { "4H", "KS", "2C", "3C", "AS" }; //test hand B

            HandCheck handA = new HandCheck(sampleHandA);
            HandCheck handB = new HandCheck(sampleHandB);

            if (handA.handPowerValue() > handB.handPowerValue())
            {
                Console.WriteLine("Hand A Wins with a " + handA.handName());
                Console.WriteLine();
                Console.WriteLine($"Hand A was a {handA.handName()} with a ranking of {handA.handPowerValue()}");
                Console.WriteLine();
                Console.WriteLine($"Hand B was a {handB.handName()} with a ranking of {handB.handPowerValue()}");

            }
            else if (handB.handPowerValue() > handA.handPowerValue())
            {
                Console.WriteLine("Hand B Wins with a " + handB.handName());
                Console.WriteLine();
                Console.WriteLine($"Hand A was a {handA.handName()} with a ranking of {handA.handPowerValue()}");
                Console.WriteLine();
                Console.WriteLine($"Hand B was a {handB.handName()} with a ranking of {handB.handPowerValue()}");
            }
            else
            {
                Console.WriteLine("Tie!");
                Console.WriteLine();
                Console.WriteLine($"Hand A was a {handA.handName()} with a ranking of {handA.handPowerValue()}");
                Console.WriteLine();
                Console.WriteLine($"Hand B was a {handB.handName()} with a ranking of {handB.handPowerValue()}");
            }

        }
    }
}
