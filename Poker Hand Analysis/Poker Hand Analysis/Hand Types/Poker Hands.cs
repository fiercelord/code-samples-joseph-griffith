﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_Hand_Analysis.Poker_Hands
{
    public class Poker_Hands : HandElements
    {
        private string name;
        private decimal powerRanking;

        protected decimal[] orderedNumericalHand;
        protected Dictionary<decimal, int> duplicateCount;
        protected string[] importedHand;

        protected Dictionary<decimal, decimal> subPowerValue = new Dictionary<decimal, decimal>
            {
                {2m, 0.001m},
                {3m,0.002m },
                {4m,0.003m },
                {5m,0.004m },
                {6m,0.005m},
                {7m,0.006m },
                {8m,0.007m },
                {9m, 0.008m },
                {10m,0.009m },
                {11m,0.010m},
                {12m, 0.011m},
                {13m,0.012m},
                {14m,0.013m}
            };

        public virtual string Name { get => name; }
        public virtual decimal PowerRanking { get => powerRanking; }

        public Poker_Hands(string[] hand)
        {
            this.importedHand = hand;
            this.orderedNumericalHand = HandOrder(hand);
            this.duplicateCount = DuplicateCount(orderedNumericalHand);
        }

        public virtual bool isThisHand()
        {
            return false;
        }

        public virtual decimal subPowerRankingCalculator()
        {
            return 0m;
        }

        public Dictionary<decimal, int> DuplicateCount(decimal[] handOrder)
        {
            Dictionary<decimal, int> duplicateAnalysis = new Dictionary<decimal, int>();
            foreach (decimal element in handOrder)
            {
                if (duplicateAnalysis.ContainsKey(element))
                {
                    duplicateAnalysis[element]++;
                }
                else
                {
                    duplicateAnalysis.Add(element, 1);
                }
            }
            return duplicateAnalysis;
        }

        public decimal[] HandOrder(string[] sampleHand)
        {
            decimal[] handOrder = new decimal[5];

            for (int i = 0; i < sampleHand.Length; i++)
            {
                if (sampleHand[i][0] == 'T')
                {
                    handOrder[i] = 10;
                    continue;
                }
                else if (sampleHand[i][0] == 'J')
                {
                    handOrder[i] = 11;
                    continue;
                }
                else if (sampleHand[i][0] == 'Q')
                {
                    handOrder[i] = 12;
                    continue;
                }
                else if (sampleHand[i][0] == 'K')
                {
                    handOrder[i] = 13;
                    continue;
                }
                else if (sampleHand[i][0] == 'A')
                {
                    handOrder[i] = 14;
                    continue;
                }
                else
                {
                    double holdingVar = Char.GetNumericValue(sampleHand[i][0]);
                    handOrder[i] = Convert.ToDecimal(holdingVar);
                }
                continue;
            }

            Array.Sort(handOrder);
            return handOrder;
        }
    }
}
