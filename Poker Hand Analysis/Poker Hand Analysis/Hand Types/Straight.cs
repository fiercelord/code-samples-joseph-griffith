﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_Hand_Analysis.Poker_Hands
{
    public class Straight: Poker_Hands
    {
        private string name = "Straight";
        private decimal powerRanking = 4m;

        public Straight(string[] hand):base(hand)
        {

        }

        public override string Name { get => name; }
        public override decimal PowerRanking { get => powerRanking; }

        public override bool isThisHand()
        {
            int orderedCount = 0;
            for (int i = 0; i < importedHand.Length - 1; i++)
            {
                if (importedHand[i] + 1 == importedHand[i + 1])
                {
                    orderedCount++;
                }
            }

            return orderedCount == 4; //Return true if the ranks are in acsending order
        }
    

        public override decimal subPowerRankingCalculator()
        {
            decimal result = 0m;
            foreach(decimal element in orderedNumericalHand)
            {
                result += subPowerValue[element];
            }
            return result;
        }
    }
}
