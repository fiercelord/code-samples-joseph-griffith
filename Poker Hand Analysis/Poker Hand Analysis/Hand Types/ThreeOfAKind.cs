﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_Hand_Analysis.Poker_Hands
{
    public class ThreeOfAKind:Poker_Hands 
    {
        private string name = "Three of a Kind";
        private decimal powerRanking = 3m;

        public ThreeOfAKind(string[] hand) : base(hand)
        {

        }

        public override string Name { get => name; }
        public override decimal PowerRanking { get => powerRanking; }

        public override bool isThisHand()
        {
            int tripleCount = 0;
            int doubleCount = 0;
            foreach(KeyValuePair<decimal,int> kvp in duplicateCount)
            {
                if(kvp.Value == 3)
                {
                    tripleCount++;
                    continue;
                }
                else if(kvp.Value == 2)
                {
                    doubleCount++;
                    continue;
                }
            }
            return tripleCount == 1 && doubleCount == 0;
        }
        public override decimal subPowerRankingCalculator()
        {
            decimal highestSecondCard = 0m;
            decimal result = 0m;
            foreach (KeyValuePair<decimal, int> kvp in duplicateCount)
            {
                if (kvp.Value == 3)
                {
                    result += subPowerValue[kvp.Key];
                    continue;
                }
                else if (kvp.Value == 0 && highestSecondCard == 0m)
                {
                    result += subPowerValue[kvp.Key]/10m;
                    highestSecondCard = 2m;
                    continue;
                }
                else if(kvp.Value == 0 && highestSecondCard != 0m)
                {
                    result += subPowerValue[kvp.Key] / 100m;
                }
            }
                return result;
        }
    }
}
