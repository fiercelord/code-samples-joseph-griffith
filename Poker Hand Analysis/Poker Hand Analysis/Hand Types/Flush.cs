﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_Hand_Analysis.Poker_Hands
{
    public class Flush: Poker_Hands
    {
        private string name = "Flush";
        private decimal powerRanking = 5m;

        public Flush(string[] hand):base(hand)
        {

        }

        public override string Name { get => name; }
        public override decimal PowerRanking { get => powerRanking; }

        public override bool isThisHand()
        {
            int countH = 0;
            int countD = 0;
            int countC = 0;
            int countS = 0;

            for (int i = 0; i < importedHand.Length; i++)
            {

                if (importedHand[i][1] == 'H')
                {
                    countH++;
                }
                else if (importedHand[i][1] == 'D')
                {
                    countD++;
                }
                else if (importedHand[i][1] == 'C')
                {
                    countC++;
                }
                else if (importedHand[i][1] == 'S')
                {
                    countS++;
                }
            }
            return (countS == 5 || countD == 5 || countH == 5 || countC == 5); //Return true if there are 5 of the same suit in the hand.
        }

        public override decimal subPowerRankingCalculator()
        {
            decimal result = 0m;
            foreach(decimal element in orderedNumericalHand)
            {
                result += subPowerValue[element];
            }

            return result;
        }
    }
}
