﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_Hand_Analysis.Poker_Hands
{
    interface HandElements
    {
        decimal PowerRanking
        {
            get;
        }

        string Name
        {
            get;
        }
        bool isThisHand();

        decimal subPowerRankingCalculator();

    }
}
