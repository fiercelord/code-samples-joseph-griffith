﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_Hand_Analysis.Poker_Hands
{
    public class Pair : Poker_Hands
    {
        private string name = "Pair";
        private decimal powerRanking = 1m;

        public Pair(string[] hand) : base(hand)
        {

        }

        public override string Name { get => name; }
        public override decimal PowerRanking { get => powerRanking; }

        public override bool isThisHand()
        {
            int doubleCount = 0;
            foreach (KeyValuePair<decimal, int> kvp in duplicateCount)
            {
                if (kvp.Value == 2)
                {
                    doubleCount++;
                    continue;
                }
            }
            return doubleCount == 1;
        }
        public override decimal subPowerRankingCalculator()
        {
            decimal pair = 0m;
            decimal[] otherCards = new decimal[3];
            int arrayCount = 0;
            foreach (KeyValuePair<decimal, int> kvp in duplicateCount)
            {
                if (kvp.Value == 2)
                {
                    pair = kvp.Key;
                    continue;
                }
                else
                {
                    otherCards[arrayCount] = kvp.Key;
                    arrayCount++;
                }
            }
            Array.Sort(otherCards);

            return subPowerValue[pair] + subPowerValue[otherCards[0]] + subPowerValue[otherCards[1]] / 10m + subPowerValue[otherCards[2]] / 100m;
        }
    }
}
