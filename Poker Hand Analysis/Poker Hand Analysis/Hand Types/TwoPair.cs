﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_Hand_Analysis.Poker_Hands
{
    public class TwoPair : Poker_Hands
    {
        private string name = "Two Pair";
        private decimal powerRanking = 2m;

        public TwoPair(string[] hand) : base(hand)
        {

        }

        public override string Name { get => name; }
        public override decimal PowerRanking { get => powerRanking; }

        public override bool isThisHand()
        {
            int tripleCount = 0;
            int doubleCount = 0;
            foreach (KeyValuePair<decimal, int> kvp in duplicateCount)
            {
                if (kvp.Value == 3)
                {
                    tripleCount++;
                    continue;
                }
                else if (kvp.Value == 2)
                {
                    doubleCount++;
                    continue;
                }
            }
            return tripleCount == 0 && doubleCount == 2;
        }
        public override decimal subPowerRankingCalculator()
        {
            decimal firstPair = 0m;
            decimal secondPair = 0m;
            decimal singleCard = 0m;
            foreach (KeyValuePair<decimal, int> kvp in duplicateCount)
            {
                if (kvp.Value == 2 && firstPair == 0m)
                {
                    firstPair = kvp.Key;
                    continue;
                }
                else if (kvp.Value == 2 && firstPair != 0m)
                {
                    secondPair = kvp.Key;
                    continue;
                }
                else
                {
                    singleCard = kvp.Key;
                    continue;
                }
            }

            if (secondPair > firstPair)
            {
                decimal holdingVar = firstPair;
                firstPair = secondPair;
                secondPair = holdingVar;
            }

            return subPowerValue[firstPair] + subPowerValue[secondPair] / 10m + subPowerValue[singleCard] / 100m;
        }
    }
}
