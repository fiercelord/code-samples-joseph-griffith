﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_Hand_Analysis.Poker_Hands
{
    public class FourOfAKind : Poker_Hands
    {
        private string name = "Four of a Kind";
        private decimal powerRanking = 7m;

        public FourOfAKind(string[] hand) : base(hand)
        {

        }

        public override string Name { get => name; }
        public override decimal PowerRanking { get => powerRanking; }

        public override bool isThisHand()
        {
            bool result = false;
            foreach (KeyValuePair<decimal, int> kvp in duplicateCount)
            {
                if (kvp.Value == 4)
                {
                    result = true;
                }
                else
                {
                    continue;
                }
            }
            return result;
        }

        public override decimal subPowerRankingCalculator()
        {
            decimal result = 0m;
            foreach(KeyValuePair<decimal,int> kvp in duplicateCount)
            {
                if(kvp.Value == 4)
                {
                    result += subPowerValue[kvp.Key];
                }
                else
                {
                    result += subPowerValue[kvp.Key] / 10m;
                }
            }
            return result;
        }
    }
}
