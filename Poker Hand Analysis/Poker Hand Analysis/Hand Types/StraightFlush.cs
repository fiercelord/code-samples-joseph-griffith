﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_Hand_Analysis.Poker_Hands
{
    public class StraightFlush: Poker_Hands
    {
        private string name = "StraightFlush";
        private decimal powerRanking = 8m;

        public StraightFlush(string[] hand):base(hand)
        {

        }

        public override string Name { get => name; }
        public override decimal PowerRanking { get => powerRanking; }

        public override bool isThisHand()
        {
            Flush flushTest = new Flush(importedHand);
            Straight straightTest = new Straight(importedHand);
            return flushTest.isThisHand() && straightTest.isThisHand();
        }
    

        public override decimal subPowerRankingCalculator()
        {
            decimal result = 0m;
            foreach(decimal element in orderedNumericalHand)
            {
                result += subPowerValue[element];
            }
            return result;
        }
    }
}
