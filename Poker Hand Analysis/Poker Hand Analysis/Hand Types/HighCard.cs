﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker_Hand_Analysis.Poker_Hands
{
    public class HighCard : Poker_Hands
    {
        private string name = "High Card";
        private decimal powerRanking = 0m;

        public HighCard(string[] hand) : base(hand)
        {

        }

        public override string Name { get => name; }
        public override decimal PowerRanking { get => powerRanking; }

        public override bool isThisHand()
        {
            int tripleCount = 0;
            int doubleCount = 0;
            foreach (KeyValuePair<decimal, int> kvp in duplicateCount)
            {
                if (kvp.Value == 3)
                {
                    tripleCount++;
                    continue;
                }
                else if (kvp.Value == 2)
                {
                    doubleCount++;
                    continue;
                }
            }
            return tripleCount == 0 && doubleCount == 0;
        }
        public override decimal subPowerRankingCalculator()
        {
            decimal[] otherCards = new decimal[5];
            int arrayCount = 0;
            foreach (KeyValuePair<decimal, int> kvp in duplicateCount)
            {
                otherCards[arrayCount] = kvp.Key;
                arrayCount++;
            }
            Array.Sort(otherCards);

            return subPowerValue[otherCards[0]] + subPowerValue[otherCards[1]] + subPowerValue[otherCards[2]] + subPowerValue[otherCards[3]] + subPowerValue[otherCards[4]];
        }
    }
}
